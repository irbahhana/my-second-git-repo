package card;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * This class contain the Card for user to click and the container for button using ArrayList
 * The id is a unique identifier for every Image that is loaded.
 * open and close variables indicate the image if it's the back or the front image that i'll impact the image flip
 * that the user will match.
 * boolean iscorrect and clicked is for indication that user has clicked or already correct
 */
public class JCard {
    private int id;
    private JButton button;
    private ImageIcon open;
    private ImageIcon closed;
    private boolean isClicked;
    private boolean isCorrect;
    public static ArrayList<JButton> buttonarray = new ArrayList<>();

    /**
     * ======= CONSTRUCTOR =======
     * @param id
     * @param button
     * @param open
     * @param closed
     */
    public JCard(int id, JButton button, ImageIcon open, ImageIcon closed){
        this.id = id;
        this.button = button;
        this.open = open;
        this.closed = closed;
        this.isClicked = false;
        buttonarray.add(this.button);
        setImage();
    }

    /**
     * @return
     */
    public int getId() {
        return id;
    }

    public void setImage(){
        button.setPreferredSize(new Dimension(150,100));    // Setting the image size for the button.
        if(isClicked){
            button.setIcon(open); // set the icon if clicked, and the image change
        }else{
            button.setIcon(closed); // if wrong it closes
        }
    }

    /**
     * ========== GETTER SETTER =========
     * @param clicked
     */
    public void setClicked(boolean clicked) {
        isClicked = clicked;
    }

    /**
     * @return isCorrect
     */
    public boolean isCorrect() {
        return isCorrect;
    }

    /**
     * @param correct
     */
    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    public JButton getButton() {
        return button;
    }

    public void openCard(){
        button.setIcon(open);
    }

    public void closeCard(){
        button.setIcon(closed);
    }

    public boolean getIsClicked() {
        return isClicked;
    }

    public boolean equaled(JCard next) {
        return this.getId() == next.getId();
    }

}
