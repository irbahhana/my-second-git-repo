package listener;

import card.JCard;
import panel.MemoryPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This class is the listener class
 */
public class Listener implements ActionListener {
    private MemoryPanel panel;
    private JButton tempbutton;
    private static JCard prevCard;
    private JCard curCard;

    /**
     * @param panel
     */
    public Listener(MemoryPanel panel){
        this.panel = panel;
    }

    /**
     * The code below is for handling/ listener
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        tempbutton = (JButton) e.getSource();
        int stepCounter = panel.getStepCounter();
        curCard = panel.findCard(tempbutton);
        curCard.setClicked(true);
        if (stepCounter == 0) firstClick();
        else if (stepCounter == 1) secondClick();
        else if (stepCounter == 2) closeOpenedCard();
        curCard.setClicked(false);
        checkWin();
    }

    /**
     * this void method is used if the user already clicked 2 button of image then it closes back again.
     */
    private void closeOpenedCard(){
        for (JCard c : panel.getArrCard()){
            if (!c.isCorrect()) c.closeCard();
        }
        panel.resetStepCounter();

    }

    /**
     * This method is for checking whether the user have matched all the card or not
     * if they won, panel.win() is called.
     */
    public void checkWin(){
        boolean isWin = true;
        for (JCard c : panel.getArrCard()) {
            isWin &= c.isCorrect();
        }
        if (isWin) panel.win();
    }

    /**
     * The method run Normal is for when the user click the first button/image.
     * it also increment the step counter to increase the steps that user has taken
     */
    private void firstClick(){
        if (curCard.getIsClicked()) curCard.openCard();
        prevCard = curCard;
        curCard.setClicked(false);
        prevCard.setClicked(false);
        panel.incStepCounter(); // incrementing the counter after the click
    }

    /**
     * this method is for the second click to match the first click.
     * this method also check whether the second click's id matched the first one
     * it also increment the Counter.
     */
    private void secondClick(){
        if (curCard.getIsClicked()) curCard.openCard();     // if its clicked then card opened
        if (curCard.getButton().equals(prevCard.getButton())) {     // if equals to previous clicked card
            curCard.closeCard();    // closed the card again
            curCard.setClicked(false);
            panel.resetStepCounter();
            panel.incCounter();     //counter incrementation
            return;
        }
        if (prevCard.equaled(curCard)) {
            correct();
            return;
        }
        curCard.setClicked(false);
        prevCard.setClicked(false);
        panel.incStepCounter();
        panel.incCounter();
    }

    /**
     * this method is for if the user match the image, the picture will erased by itself and make the button
     * disappear.
     */
    private void correct(){
        prevCard.getButton().setVisible(false); // set visible method set to false to make the button disappear
        prevCard.setCorrect(true);
        curCard.getButton().setVisible(false); // set visible method set to false to make the button disappear
        curCard.setCorrect(true);
        panel.incCounter();
        panel.resetStepCounter();
    }

}