package panel;

import card.JCard;
import listener.Listener;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * This is the panel class.
 * main panel button panel and welcome label are methods called from the imported class for the panel
 * total counter is for displaying how much steps has the player taken.
 */
public class MemoryPanel {
    private JPanel mainPanel;
    private JPanel buttonPanel;
    private JLabel welcomeLabel;
    private int totalCounter;
    private int stepCounter;
    private ArrayList<JCard> arrCard = new ArrayList<>();
    private Listener command = new Listener(this);

    /**
     * CONSTRUCTOR
     */
    public MemoryPanel() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        buttonPanel = cardButton();
        mainPanel.add(buttonPanel, BorderLayout.PAGE_START);
        welcomeLabel = new JLabel("GOOD LUCK !");
        mainPanel.add(welcomeLabel, BorderLayout.WEST);
        mainPanel.add(helperButton(), BorderLayout.EAST);
        welcome(); // adding welcome message to let player know wwassuppz
    }

    /**
     * This method is for button and image import.
     * it use 6 rows and 6 columns,
     * the image is taken from folder listed in the ImageIcon
     * there also a loop to add images for the hidden picture
     * @return
     */
    private JPanel cardButton() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(6, 6));  // rows and columns setter
        ImageIcon openedCard;
        ImageIcon closedCard = new ImageIcon("/Users/user/Desktop/2nd/Assignment/assignment-4/img/nba.png"); // FILE NAME HERE
        closedCard = iconConfig(closedCard); // closed card setted to fit the buttons.

        // loop for adding the image to the buttons.
        int j = 1;
        for (int i = 1; i <= 36; i++) {
            if (j > 18) j = 1;
            JButton button = new JButton();
            button.addActionListener(command);
            openedCard = new ImageIcon("/Users/user/Desktop/2nd/Assignment/assignment-4/img/imgback/" + j + ".png");   // FILE NAME HERE
            openedCard = iconConfig(openedCard);
            JCard temp = new JCard(j, button, openedCard, closedCard);
            arrCard.add(temp);
            j++;
        }
        shuffleImage();  // method for shuffling the image
        for (JButton b : JCard.buttonarray) {   // adding to panel
            panel.add(b);
        }
        return panel;
    }

    /**
     * this method is for the image to fit the button.
     * refrence : https://coderanch.com/t/331731/java/Resize-ImageIcon
     *
     * @param icon
     * @return
     */
    private ImageIcon iconConfig(ImageIcon icon) {
        Image img = icon.getImage();
        Image imageres = img.getScaledInstance(100, 80, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(imageres);
    }

    /**
     * this method is for the RESTART and EXIT button
     * if it is pressed, the restart button will reset the gmae and the counter
     * if the exit button is pressed, it will close the application.
     *
     * @return
     */
    private JPanel helperButton() {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        JButton restart = new JButton("Restart");
        restart.addActionListener(e -> restart());
        JButton exit = new JButton("Exit");
        exit.addActionListener(e -> System.exit(0));
        panel.add(restart);
        panel.add(exit);
        return panel;
    }

    /**
     * the void method restart is for the game to reset itself and also reset the program counter.
     * when the user pressed the restart button, a message will pop up to double check whether
     * the user want to reset the game or not.
     */
    private void restart() {
        int restart = JOptionPane.showConfirmDialog(mainPanel, "Thanks for playing !! \n Do you want to restart " +
                        "the game ?",
                "Restart", JOptionPane.YES_NO_OPTION); // using yes no button provided by imported class.
        if (restart == JOptionPane.YES_OPTION) {
            mainPanel.remove(buttonPanel);
            mainPanel.revalidate();
            mainPanel.repaint();
            arrCard.clear();
            JCard.buttonarray.clear();
            mainPanel.add(cardButton(), BorderLayout.PAGE_START);
            resetStepCounter();
            resetCounter();
        }
    }

    /**
     * this Method is to find the card
     * @param button
     * @return
     */
    public JCard findCard(JButton button) {
        for (JCard c : arrCard) {
            if (button == c.getButton()) {
                return c;
            }
        }
        return null;
    }

    /**
     * Using collections, the card can be shuffled using method shuffle.
     */
    private void shuffleImage() {
        Collections.shuffle(JCard.buttonarray);
    }

    /**
     * reseting the counter.
     */
    private void resetCounter() {
        totalCounter = 0;
        welcomeLabel.setText("Number of tries: " + totalCounter);
    }

    /**
     * Method win if the player done matching all the image given
     * The method will differentiate player's winning message.
     * if player win below 40 steps, the message will be different from the player that wins with steps above 50.
     */
    public void win() {
        if (totalCounter < 40) {
            JOptionPane.showMessageDialog(mainPanel, "           WIR GRATULIEREN!\n YOU BEAT THE GAME EASILY" +
                    " WITH " + getTotalCounter() + " STEPS", "CONGRATULATION!", JOptionPane.INFORMATION_MESSAGE);
        } else if (41 < totalCounter)
            JOptionPane.showMessageDialog(mainPanel, "            HORRAY !!!!\n YOU FINISH THE GAME WITH " +
                    getTotalCounter() + " STEPS", "Well Done !!", JOptionPane.INFORMATION_MESSAGE);
        restart(); // calling this method to auto restart the game after winning
    }

    /**
     * This method will display a message as a guide for the player
     */
    public void welcome(){
        JOptionPane.showMessageDialog(mainPanel,"                                                    " +
                " Welcome!!" +
                "\nThe game" +
                "is a match-pair memory game where the player will have to match animal\n" +
                "characters that are hidden behind 6x6 grid tiles. There are 18 animal\n" +
                "characters hidden in the game and player must find matching characters by\n" +
                "checking the tiles. When player found matching pair of characters, then\n" +
                "the tiles that hid the characters will disappear/gone invisible.\n" +
                "\n" +
                "Player can only open at most 2 tiles. If there are 2 open tiles and the\n" +
                "player wanted to open another tile, then previous 2 open tiles will be\n" +
                "closed.\n "+"                                    "+
                "=== PRESS OK TO START ===","Match-Pair Memory Game",JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * ========== GETTER SETTER ==========
     * @return
     */
    public int getStepCounter() {
        return stepCounter;
    }

    public void incStepCounter(){
        stepCounter++;
    }

    public void resetStepCounter(){
        stepCounter = 0;
    }

    public ArrayList<JCard> getArrCard() {
        return arrCard;
    }
    public JPanel getMainPanel() {
        return mainPanel;
    }

    public void incCounter(){
        welcomeLabel.setText("Number of tries: " + ++totalCounter);
    }
    public int getTotalCounter() {
        return totalCounter;
    }
}
