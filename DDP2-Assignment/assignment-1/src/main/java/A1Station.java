import java.util.Scanner;

public class A1Station{

    private static final double THRESHOLD = 250; 
    public static TrainCar x;
    static int valct;

   public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the number of cats: ");
        valct = Integer.parseInt(input.nextLine());
        int counter = 0;
        TrainCar front=null;
        
       for(int catval=0;catval<valct;catval++){
            // Scanner input = new Scanner(System.in);
            String num;
            int wgtinp,hgtinp;
            String rdinp;
            String[] rdinfo;
            rdinp = input.nextLine();
            rdinfo = rdinp.split(",");
            num = (rdinfo[0]);
            wgtinp = Integer.parseInt(rdinfo[1]);
            hgtinp = Integer.parseInt(rdinfo[2]);

        WildCat cat = new WildCat(num, wgtinp, hgtinp);
            if(front==null){
                x= new TrainCar(cat);
                front =x;
            } else {
                x= new TrainCar(cat,front);        
                front=x;     
            }
            counter++;
            if (front.computeTotalWeight() > THRESHOLD || catval ==(valct-1)) {
                //break;
                System.out.println("The train departs to Javari Park");
                 System.out.print("[LOCO]<--");
                 front.printCar();
                 System.out.println("");
                 double avgmass = front.computeTotalMassIndex()/counter;
                 System.out.print("Average mass index of all cats: ");
                 System.out.print(String.format("%.2f",avgmass));
                counter=0;
                 front=null;
                 System.out.println("");

            System.out.print("In average,the cats in the train are ");
            if(avgmass<18.5){
                System.out.print("* underweight *");
                System.out.println("");
            }
            else if(18.5 <=  avgmass && avgmass   <25){
                System.out.print("*normal*");
                System.out.println("");
            }

            else if(25 <= avgmass && avgmass <30){
                System.out.print("*overweight*");
                System.out.println("");
            }
            else if ( 30 <= avgmass ) {
                System.out.print("*obese*");
                System.out.println("");
            }
                }
            }
    }
}