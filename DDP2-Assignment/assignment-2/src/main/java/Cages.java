import java.util.ArrayList;
public class Cages {

    //Instance Variable of Cages
    private ArrayList<Animal> listBefore;
    private Animal listAfter[][];
    private int size;

    //Constructor of Cages
    Cages(ArrayList<Animal> listBefore) {
        this.listBefore = listBefore;
    }


    public void arrange() {
        int indeks = 0;

        //if Cage is already full
        if (listBefore.size() > ((listBefore.size() / 3) * 3)) {
            size = listBefore.size() / 3;
            indeks = 3 - (listBefore.size() - size * 3);
        } else {
            size = listBefore.size() / 3 - 1;
        }
        listAfter = new Animal[3][size + 1];
        int indeks2 = 0;

        //if animal less than 3
        if (size == 0) {
            for (int a = 0; a < listBefore.size(); a++) {
                listAfter[a][0] = listBefore.get(a);
            }
        } else {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < size + 1; j++) {
                    if (j == size && i < indeks) {
                        break;
                    } else {
                        listAfter[i][j] = listBefore.get(indeks2);
                        indeks2++;
                    }
                }
            }
        }

        System.out.println("location: " + listAfter[0][0].getPlace());
        printAnimal(listAfter, size);
        System.out.println("\n");
    }

    //Arranging the cage
    public void afterArrange() {
        Animal listAfterArrange[][];
        listAfterArrange = new Animal[3][size + 1];
        int temp1 = 1;
        for (int i = 2; i >= 0; i--) {
            int temp2 = size;
            if (temp1 == -1) {
                temp1 = 2;
            }
            for (int j = 0; j < size + 1; j++) {
                listAfterArrange[i][j] = listAfter[temp1][temp2];
                temp2--;
            }
            temp1--;
        }
        System.out.println("After rearrangement...");
        printAnimal(listAfterArrange, size);
        System.out.println();
    }

    //Printing the Cage using loop
    private void printAnimal(Animal listAnimal[][], int size) {
        for (int k = 2; k >= 0; k--) {
            System.out.print("Level " + (k + 1) + ": ");
            for (int l = 0; l < size + 1; l++) {
                if (listAnimal[k][l] != null) {
                    System.out.print(listAnimal[k][l].getName() + "(" +
                            listAnimal[k][l].getLength() + " - " +
                            listAnimal[k][l].getCageCode() + "), ");
                }
            }
            System.out.print("\n");
        }
    }
}
