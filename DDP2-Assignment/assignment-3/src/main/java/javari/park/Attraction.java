package javari.park;
import javari.animal.*;
import java.util.ArrayList;
import java.util.List;

public class Attraction implements SelectedAttraction{
  private String name,type;
  private List<Animal> performers = new ArrayList<Animal>();  // List of animals that can do attraction

  public Attraction (String type, String name){
    this.type = type;
    this.name = name;
  }

  public String getName(){return name;}
  public String getType(){return type;}
  public List<Animal> getPerformers(){return performers;} // select an animal
  public boolean addPerformer (Animal performer){
    if(performer.getType().equals(type) && performer.isShowable()){
      performers.add(performer);
      return true;
    }
    else{
      return false;
    }
  }
}
