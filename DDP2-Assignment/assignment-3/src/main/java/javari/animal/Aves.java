package javari.animal;
public class Aves extends Animal{
  private boolean islayingeggs=false;

  public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight,String islayingeggs,Condition condition){
                  super(id,type,name,gender,length,weight,condition);
                  if(islayingeggs.equals("laying eggs")){
                    this.islayingeggs = true;					
                  }
                }

  public boolean specificCondition(){
    // Special condition for aves because aves lays eggs
    // when lay eggs they cant perform
    if(!islayingeggs){
      return true;
    }
    else{
      return false;
    }
  }


}
