package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
public class ReaderAnimalCategories extends CsvReader{
  private List<String> categories = new ArrayList<String>();
  public ReaderAnimalCategories(Path File) throws IOException {
    super(File);
  }
  public long countValidRecords(){
    for(String record : lines){
      String[] data = record.split(COMMA);
      try{
        for(String categorie : categories){
          if(data[1].equals(categorie)){
            throw new Exception();
          }
        }
        categories.add(data[1]);
      }
      catch(Exception e){
        continue;
      }

  }

    return (long) categories.size();
  }

  public long countInvalidRecords(){
    long invalid =0;
    for(String record:lines){
      String[] data = record.split(COMMA);
      if(data.length<2){
        invalid+=1;
      }
    }
    return invalid;
  }

  public List<String> getCategories(){
    return categories;
  }

}
