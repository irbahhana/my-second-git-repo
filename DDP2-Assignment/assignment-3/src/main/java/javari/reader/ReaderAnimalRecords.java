package javari.reader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
public class ReaderAnimalRecords extends CsvReader{
  public ReaderAnimalRecords(Path File)throws IOException{
    super(File);
  }
  // counting how many records that is valid from the csv file given in data
  // Animal Records
  public long countValidRecords(){
    long valid =0;
    for(String record : lines){
      String[] data = record.split(COMMA);  // split by coma
      if (data.length == 8){
        valid+=1;
      }
    }
    return valid;
  }

  // count how many invalid records that given is given from the data
  // Animal Records
  public long countInvalidRecords(){
    long invalid =0;
    for(String record:lines){
      String[] data = record.split(COMMA);  // split by coma
      if(data.length!=8){
        invalid+=1;
      }
    }
    return invalid;
  }

}
