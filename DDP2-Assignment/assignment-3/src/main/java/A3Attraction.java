import javari.animal.*;
import javari.park.*;
import javari.reader.*;
import java.util.*;
import java.io.*;
import java.nio.file.Paths;

public class A3Attraction{
  public static void main(String args[]){
    List<Animal> listanimals = new ArrayList<Animal>();
    List<Attraction> listattractions = new ArrayList<Attraction>();
    List<Section> listSections = new ArrayList<Section>();
    ArrayList<Visitor> registeredVisitor = new ArrayList<>();
    int id =1;
    long sectionnumber=0;
    String directory ="";
    Scanner input = new Scanner(System.in);
    CsvReader categories,records,attractions;
    ReaderSections sections;
    while(true){
      System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        try{
          File CsvAnimalCategories = new File(directory+"animals_categories.csv");
          File CsvAnimalRecords = new File(directory+"animals_records.csv");
          File CsvAttractions = new File(directory+"animals_attractions.csv");
          File CsvSections = new File(directory+"animals_categories.csv");

          categories = new ReaderAnimalCategories(CsvAnimalCategories.toPath());
          records = new ReaderAnimalRecords(CsvAnimalRecords.toPath());
          attractions = new ReaderAttractions(CsvAttractions.toPath());
          sections = new ReaderSections(CsvSections.toPath());
          System.out.println("... Loading... Success... System is populating data...\n");
          System.out.println("Found _"+sections.countValidRecords()+"_ valid sections and _"+sections.countInvalidRecords()+"_ invalid sections");
          System.out.println("Found _"+attractions.countValidRecords()+"_ valid attractions and _"+attractions.countInvalidRecords()+"_ invalid attractions");
          System.out.println("Found _"+categories.countValidRecords()+"_ valid animal categories and _"+categories.countInvalidRecords()+"_ invalid animal categories");
          System.out.println("Found _"+records.countValidRecords()+"_ valid animal records and _"+records.countInvalidRecords()+"_ invalid animal records\n");
          sectionnumber=sections.countValidRecords();

          for(String section : sections.getSections()){ // Selections Handling
            listSections.add(new Section(section));
          }

          for(String categorie : categories.getLines()){ // Categories Handling
            String[] data = categorie.split(",");
            for(Section section : listSections){
              if(data[2].equals(section.getName())){
                section.addAnimal(data[0]);
              }
            }
          }

          for(String attraction : attractions.getLines()){ // Attraction Handling
            String[] data = attraction.split(",");
            listattractions.add(new Attraction(data[0],data[1]));
          }
          for(String record : records.getLines() ){
            String [] data = record.split(",");
            if(data[1].equals("Cat") || data[1].equals("Lion") || data[1].equals("Hamster") || data[1].equals("Whale")){
              listanimals.add(new Mammals(Integer.parseInt(data[0]),data[1],data[2],Gender.parseGender(data[3]),Double.parseDouble(data[4]),Double.parseDouble(data[5]),data[6],Condition.parseCondition(data[7])));
            }
            if(data[1].equals("Parrot") || data[1].equals("Eagle")){
              listanimals.add(new Aves(Integer.parseInt(data[0]),data[1],data[2],Gender.parseGender(data[3]),Double.parseDouble(data[4]),Double.parseDouble(data[5]),data[6],Condition.parseCondition(data[7])));
            }
            if(data[1].equals("Snake")){
              listanimals.add(new Reptile(Integer.parseInt(data[0]),data[1],data[2],Gender.parseGender(data[3]),Double.parseDouble(data[4]),Double.parseDouble(data[5]),data[6],Condition.parseCondition(data[7])));
            }
          }

          for(Attraction attraction : listattractions){
            for(Animal animal : listanimals){
              attraction.addPerformer(animal);
            }
          }
          break;


        }
        catch (IOException e){
          System.out.println("... Opening default section database from data. ... File not found or incorrect file!\n");
          System.out.print("Please provide the source data path: \n");
          directory = input.nextLine();

        }
      }
      System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
      System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu\n");
      while(true){
        int index = 1;
        System.out.println("Javari Park has "+sectionnumber+" sections:");
        for(Section section : listSections){
          System.out.println(index+". "+section.getName());
          index++;
        }
        System.out.print("Please choose your preferred section (type the number): ");
        int SectionMenu = Integer.parseInt(input.nextLine());
        index=1;
        System.out.println("\n--" + listSections.get(SectionMenu-1).getName() + "--");
        for(String type : listSections.get(SectionMenu-1).getAnimal()){
            System.out.println(index + ". " + type);
            index++;
          }
        System.out.print("Please choose your preferred animals (type the number): ");

        String AnimalMenu = input.nextLine(); // Choose user input and verify the type of animals
        if(AnimalMenu.equals("#")){
          System.out.println();
          continue;
        }
        String animalType = listSections.get(SectionMenu-1).getAnimal().get(Integer.parseInt(AnimalMenu)-1);

        boolean available= false;
        for(Attraction atraksi : listattractions){
          if(!atraksi.getPerformers().isEmpty()){
            for(Animal animal : atraksi.getPerformers()){
              if(animal.getType().equals(animalType)){
                available = true;
              }
            }
          }
        }
        if(!available){
          System.out.println("\nUnfortunately, no " + animalType + " can perform any attraction, please choose other animals\n");
          continue;
        }
        System.out.println("\n---" + animalType + "---\n" +"Attractions by " + animalType + ":");
        index=1;
        ArrayList<Attraction> option = new ArrayList<>();
        for(Attraction atraksi : listattractions){
          if(!atraksi.getPerformers().isEmpty()){
              if(atraksi.getType().equals(animalType)){
                option.add(atraksi);
                System.out.println(index + ". " + atraksi.getName());
                index++;
              }
          }
        }

        System.out.print("Please choose your preferred attractions (type the number): ");
        String chooseAttraction = input.nextLine();

        // Main Menu selection
        if(chooseAttraction.equals("#")) continue; 

        Attraction chosenAttraction = option.get(Integer.parseInt(chooseAttraction)-1);
        System.out.print("\nWow, one more step,\n" +"please let us know your name: ");
        String visitorName = input.nextLine();

        System.out.print("\nYeay, final check!\n" +"Here is your data, and the attraction you chose:\n" +"Name: " + visitorName + "\n" +
                    "Attractions: " + chosenAttraction.getName() + " -> " + animalType + "\n" +
                    "With: ");

        int i=0;
        for(Animal performers : chosenAttraction.getPerformers()){
                if(performers.getType().equals(animalType)){
                    System.out.print(chosenAttraction.getPerformers().get(i).getName());
                    if(i!=chosenAttraction.getPerformers().size()-1) System.out.print(", ");
                }
                i++;
            }
        System.out.println("\n");
        System.out.print("Is the data correct? (Y/N): ");
        String confirmation = input.nextLine();
        if(confirmation.equals("N")){
            System.out.println("Unregistering ticket. Back to main menu.\n");
            continue;
        }
        boolean alreadyRegister = false;
        for(Visitor visitor : registeredVisitor){
            if(visitor.getVisitorName().equals(visitorName)){
                visitor.addSelectedAttraction(chosenAttraction);
                alreadyRegister = true;
            }
        }
        if(!alreadyRegister){
          registeredVisitor.add(new Visitor(id,visitorName));
          id++;
        }
        System.out.print("\nThank you for your interest. Would you like to register to other attractions? (Y/N): ");
        String anotherTicket = input.nextLine();

        if(anotherTicket.equals("Y")) continue;
        System.out.println("... End of program");
        break;

      }
  }
}
